1.3
  FIX : Viper4Android and fos_wallpapers folder are not writable.
  ADD: Google DNS.
  ADD: Substratum theme engine.
  ADD: Busybox, that will fix SuperSU installation for some poeple.
  Some improvements.

1.2.1
  Fix system verification
  Fix inverted checkbox
  Fix recovery restoration
  No more OnePlus recovery coming from the dark side
  Fix permissions in Viper4Android folder

1.2
  Adding option for user to disable system verification after installation.
  Adding backup/restore script for addon.d.
  The both mean a better support for dirty flash.
      |-> I do not recommend any dirty flash!
  Adding FreedomOS wallpapers designed by @badboy47.
      |-> Available in /sdcard/FreedomOS_wallpaper.
  Updated to OxygenOS 3.2.2:
      - Improved notification management in doze.
      - Addressed alert slider/silent mode issue.
      - Disabled fingerprint sensor while in pocket.
      - Added NFC toggle in quick settings.
      - Improved noise cancellation in video recording.
      - Updated 4K video recording codec.
      - Added latest security patches and various optimizations.

1.1.2
  Update to OxygenOS 3.2.1.
      - Fixed some notification issues
      - Addressed SIM recognition issue
      - Enabled sRGB mode in developer options
      - Improved RAM management
      - Improved GPS performance
      - Enhanced audio playback quality
      - Updated custom icon packs
      - Improved camera quality/functionality
      - Fixed some issues in Gallery
      - Implemented latest Google security patches
      - Fixed bugs in Clock/Music apps
  Fix wrong indicated version of SuperSU in aroma.

1.1.1
  Update French, few lines were missing.
  New Viper4Android Profiles and IRS, A.R.I.S.E, Rika and FreedomOS.
  Remove reboot option at the beginning of the aroma.
  Update to SuperSU v2.76.
  Goodbye app limitation in RAM, dummy on 1.1.
  No more Xposed framework, flash the zip file by yourself, apk still there.
  Welcome to RRO Layers Manager
  Add missing information in aroma @TimV_
  New splash screen @badboy47
  Status bar optimized
  Aroma ready for a kernel, released planned in august, stay tuned :) .
  Remove Bugle in aroma, deleted in OxygenOS 3.2.0
  Tallback & TTS removed from aroma.

1.1
  Update Adaway 3.1.1 to 3.1.2.
  Update host file (58,692 additions, 15,444 deletions)
  Splash screen for aroma, thanks @badboy47 for the original work.
  Add 13 more options for debloat
  Update to OxygenOS 3.2 :
        -  Enabled sRGB mode in developer options
        -  Improved RAM management
        -  Improved GPS performance
        -  Enhanced audio playback quality
        -  Updated custom icon packs
        -  Fixed some issues with notifications
        -  Improved camera quality/functionality
        -  Fixed some issues in Gallery
        -  Latest Google security patches
        -  Fixed bugs in Clock/Music apps

1.0.1
  Update French, few lines were missing.
  More DPI choice, 500-420 > 500-350.
  Add Android Pay and Swiftkey in Aroma Installer.
  Better uninstaller for Live wallpapers.
  Fix uninstaller for some apps (Like Play Music, Play Films...).
  Update hosts file.
  And more.. check github.

1.0
  Initial release
